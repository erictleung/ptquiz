# ptquiz

Simple Python quiz on the periodic table of elements.

Inspired by https://www.sporcle.com/games/g/elements.

## Requirements

- Python 3

## Usage

```bash
python ptquiz.py
```
