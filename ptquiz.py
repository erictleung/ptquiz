import time

# Up to date as of 2020-06-19
# https://en.wikipedia.org/wiki/Periodic_table
elements = [
    'hydrogen', 'helium', 'lithium', 'beryllium', 'boron', 'carbon',
    'nitrogen', 'oxygen', 'fluorine', 'neon', 'sodium', 'magnesium',
    'aluminum', 'silicon', 'phosphorus', 'sulfur', 'chlorine', 'argon',
    'potassium', 'calcium', 'scandium', 'titanium', 'vanadium', 'chromium',
    'manganese', 'iron', 'cobalt', 'nickel', 'copper', 'zinc', 'gallium',
    'germanium', 'arsenic', 'selenium', 'bromine', 'krypton', 'rubidium',
    'strontium', 'yttrium', 'zirconium', 'niobium', 'molybdenum', 'technetium',
    'ruthenium', 'rhodium', 'palladium', 'silver', 'cadmium', 'indium', 'tin',
    'antinomy', 'tellurium', 'iodine', 'xenon', 'cesium', 'barium',
    'lanthanum', 'cerium', 'praseodymium', 'neodymium', 'promethium',
    'samarium', 'europium', 'gadolinium', 'terbium', 'dysprosium', 'holmium',
    'erbium', 'thulium', 'ytterbium', 'lutetium', 'hafnium', 'tantalum',
    'tungsten', 'rhenium', 'osmium', 'iridium', 'platinum', 'gold', 'mercury',
    'thallium', 'lead', 'bismuth', 'polonium', 'astatine', 'radon', 'francium',
    'actinium', 'thorium', 'protactinium', 'uranium', 'neptunium', 'plutonium',
    'americium', 'curium', 'berkelium', 'californium', 'einsteinium',
    'fermium', 'mendelevium', 'nobelium', 'lawrencium', 'rutherfordium',
    'dubnium', 'seaborgium', 'bohrium', 'hassium', 'meitnerium',
    'darmstadtium', 'roentgenium', 'copernicium', 'nihonium', 'flerovium',
    'moscovium', 'livermorium', 'tennessine', 'oganesson'
]
correct = []
done = ['bye', 'done', 'thanks', 'goodbye', 'no', 'gone']

start = time.time()  # Start quiz time
while True:
    guess = input('Guess?: ')

    if (guess.lower() in elements) and (guess.lower() not in correct):
        correct.append(guess)

        print(correct)
        print(f'{len(correct)} out of {len(elements)} correct')
        print(f'{round(time.time()-start)} seconds elapsed\n')

    elif len(guess) == len(elements):
        print('Congratulations!')
        print(f'You finished with {round(time.time()-start)} seconds elapsed.')

    elif guess.lower() in done:
        break
